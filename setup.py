from setuptools import setup 
import json

with open("requirements.json")as f:
    data = json.load(f)

gcp_dependencies = data["gcp"]
aws_dependecies = data["aws"]
all_dependencies = gcp_dependencies + aws_dependecies

setup( 
	name='cloud_utils', 
	version='0.1', 
	description='Cloud specific utilities', 
	author='Gunasekar Velraj', 
	author_email='karthiguna065150@gmail.com', 
	packages=['cloud_utils'], 
	install_requires=[],
    extras_require={
        "gcp": gcp_dependencies,
        "aws": aws_dependecies,
        "all": all_dependencies
    }
	
) 
